![ALT](https://media0.giphy.com/headers/GitHub/w8ZJLtJbmuph.gif)

[![GitLab](https://badgen.net/badge/icon/gitlab?icon=gitlab&label)](https://https://gitlab.com/) [![GitHub](https://badgen.net/badge/icon/github?icon=github&label)](https://github.com)
![Terminal](https://badgen.net/badge/icon/terminal?icon=terminal&label) [![Windows](https://badgen.net/badge/icon/windows?icon=windows&label)](https://microsoft.com/windows/)

# Pet Shop API

----
Mini project Pet Shop API ini saya buat dengan menggunakan SpringBoot dan menerapkan Spring IoC, Java Stream, dan Native Query SQL. Mini Project ini saya buat untuk mengimplementasikan sebuah aplikasi Pet Shop, dimana Customer bisa melakukan transaksi produk kebutuhan Hewan Peliharaan seperti Kucing dan yang lainnya melalui Aplikasi ini.

## Set Up & Configuration
Sebelum menggunakan aplikasi, harus dilakukan dulu set up dan konfigurasi pada file application.properties untuk akun RDBMS, url, driver dan yang lainnya, sebagai berikut:

```
server.port=8080
spring.datasource.username=postgres
spring.datasource.password=password
spring.datasource.url=jdbc:postgresql://localhost:5432/pet_shop_db
spring.datasource.driver-class-name=org.postgresql.Driver
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.show-sql=true
```

dan Konfigurasi pada file pom.xml untuk memasukkan dependecies yang dibutuhkan dan beberapa konfigurasi lainnya sebagai berikut :
```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.7.13</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>com.fredy</groupId>
	<artifactId>pet_shop</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>pet_shop</name>
	<description>pet_shop</description>
	<properties>
		<java.version>11</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-ui</artifactId>
			<version>1.6.15</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.9.1</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<excludes>
						<exclude>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
						</exclude>
					</excludes>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>

```

## Features
Pada aplikasi ini terdapat beberapa fitur yang tersedia, sebagai berikut:
1. Create Supervisor Data
2. Find All Supervisors Data
3. Find Supervisor Data By Id
4. Update Data Supervisor
5. Delete Data Supervisor Data By Id
6. Create Admin Data
7. Find All Admins Data
8. Find Admin Data By Id
9. Update Data Admin
10. Delete Data Admin Data By Id
11. Find Admin Data By Email using Native SQL Query
12. Search Admin Data By Parameter Name OrEmail
13. Create Customer Data
14. Find All Customers Data
15. Find Customer Data By Id
16. Update Data Customer
17. Delete Data Customer Data By Id
18. Find Customer Data By Email using Native SQL Query
19. Search Customer Data By Parameter Name OrEmail
20. Create Product Data
21. Find All Products Data
22. Find Product Data By Id
23. Update Data Product
24. Delete Data Product Data By Id
25. Search CustProductomer Data By Keyword Product Name Or Product Description using Native SQL Query
26. Create Order Transaction
27. Find Order Data By Id
28. Find All Order Transactions
29. Search Order Transaction By Date Range using Native SQL Query

Semua Feature tersebut dapat digunakan melalui API yang ada di folder Controller. API ini akan melakukan request HTTP seperti POST, GET, PUT, atau DELETE untuk mengakses dan memanipulasi data di database. Dalam konteks aplikasi backend, API (Application Programming Interface) ini adalah sekumpulan endpoint atau titik akses yang memungkinkan klien (seperti aplikasi frontend atau layanan lain) berkomunikasi dengan server backend.
