package com.fredy.pet_shop.controller;

import com.fredy.pet_shop.entity.Admin;
import com.fredy.pet_shop.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/admins")
public class AdminController {

    private final AdminService adminService;

    @GetMapping
    public List<Admin> getAllAdmins() {
        return adminService.getAll();
    }

    @PostMapping
    public Admin createAdmin(@RequestBody Admin admin) {
        return adminService.create(admin);
    }

    @GetMapping("/{adminId}")
    public Admin getAdminById(@PathVariable String adminId) {
        return adminService.getById(adminId);
    }

    @PutMapping("/{adminId}")
    public Admin updateAdmin(@RequestBody Admin admin) {
        return adminService.update(admin);
    }

    @DeleteMapping("/{adminId}")
    public void deleteAdmin(@PathVariable String adminId) {
        adminService.deleteById(adminId);
    }

    @GetMapping("/email/{email}")
    public Admin getAdminByEmail(@PathVariable String email) {
        return adminService.getByEmail(email);
    }

    @GetMapping("/search")
    public List<Admin> searchAdmins(@RequestParam String keyword) {
        return adminService.searchAdmins(keyword);
    }
}
