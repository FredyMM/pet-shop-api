package com.fredy.pet_shop.controller;

import com.fredy.pet_shop.dto.OrderDetailDTO;
import com.fredy.pet_shop.dto.OrderRequestDTO;
import com.fredy.pet_shop.entity.Customer;
import com.fredy.pet_shop.entity.Order;
import com.fredy.pet_shop.entity.OrderDetail;
import com.fredy.pet_shop.entity.Product;
import com.fredy.pet_shop.repository.CustomerRepository;
import com.fredy.pet_shop.repository.ProductRepository;
import com.fredy.pet_shop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/orders")
public class OrderController {

    private final OrderService orderService;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;

    @GetMapping
    public List<Order> findAllOrders() {
        return orderService.getAllOrders();
    }

    @PostMapping
    public Order createOrder(@RequestBody OrderRequestDTO request) {
        try {
            String customerId = request.getCustomerId();
            List<OrderDetailDTO> orderDetailDTOs = request.getOrderDetails();

            // Validate customer ID
            Customer customer = customerRepository.findById(customerId)
                    .orElseThrow(() -> new EntityNotFoundException("Customer not found with ID: " + customerId));

            List<OrderDetail> orderDetails = new ArrayList<>();
            double grandTotal = 0.0;

            for (OrderDetailDTO detailDTO : orderDetailDTOs) {
                Product product = productRepository.findById(detailDTO.getProductId())
                        .orElseThrow(() -> new EntityNotFoundException("Product not found with ID: " + detailDTO.getProductId()));

                if (product.getStock() < detailDTO.getQuantity()) {
                    throw new RuntimeException("Insufficient stock for product: " + product.getName());
                }

                grandTotal += detailDTO.getSubtotal();

                OrderDetail orderDetail = OrderDetail.builder()
                        .quantity(detailDTO.getQuantity())
                        .subtotal(detailDTO.getSubtotal())
                        .product(product)
                        .build();

                orderDetails.add(orderDetail);
                product.setStock(product.getStock() - detailDTO.getQuantity());
                productRepository.save(product);
            }

            Order order = Order.builder()
                    .customerId(customerId)
                    .transactionDate(LocalDateTime.now())
                    .grandTotal(grandTotal)
                    .listOrderDetails(orderDetails)
                    .build();

            order = orderService.createOrder(customer.getCustomerId(), orderDetails);
            return order;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to create order", e);
        }
    }

    @GetMapping("/{orderId}")
    public Order findOrderById(@PathVariable String orderId) {
        return orderService.getOrderById(orderId);
    }

    @GetMapping("/search")
    public List<Order> searchOrdersByDateRange(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                               @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return orderService.searchOrdersByDateRange(startDate, endDate);
    }
}
