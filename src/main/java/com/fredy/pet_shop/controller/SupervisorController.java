package com.fredy.pet_shop.controller;

import com.fredy.pet_shop.entity.Supervisor;
import com.fredy.pet_shop.service.SupervisorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/supervisors")
public class SupervisorController {

    private final SupervisorService supervisorService;

    @GetMapping
    public List<Supervisor> getAllSupervisors() {
        return supervisorService.getAll();
    }

    @PostMapping
    public Supervisor createSupervisor(@RequestBody Supervisor supervisor) {
        return supervisorService.create(supervisor);
    }

    @GetMapping("/{supervisorId}")
    public Supervisor getSupervisorById(@PathVariable String supervisorId) {
        return supervisorService.getById(supervisorId);
    }

    @PutMapping("/{supervisorId}")
    public Supervisor updateSupervisor(@RequestBody Supervisor supervisor) {
        return supervisorService.update(supervisor);
    }

    @DeleteMapping("/{supervisorId}")
    public void deleteSupervisor(@PathVariable String supervisorId) {
        supervisorService.deleteById(supervisorId);
    }
}
