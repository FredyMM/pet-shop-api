package com.fredy.pet_shop.repository;

import com.fredy.pet_shop.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<Admin, String> {

    @Query(value = "SELECT * FROM m_admin WHERE email = :email", nativeQuery = true)
    Optional<Admin> findAdminByEmail(@Param("email") String email);
}
