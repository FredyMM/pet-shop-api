package com.fredy.pet_shop.repository;

import com.fredy.pet_shop.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {

    @Query(value = "SELECT * FROM m_customer WHERE email = :email", nativeQuery = true)
    Optional<Customer> findCustomerByEmail(@Param("email") String email);
}
