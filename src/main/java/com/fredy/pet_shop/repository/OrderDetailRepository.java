package com.fredy.pet_shop.repository;

import com.fredy.pet_shop.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, String> {

    @Query(value = "SELECT * FROM t_order_detail WHERE order_id = :orderId", nativeQuery = true)
    List<OrderDetail> findOrderDetailsByOrderId(@Param("orderId") Long orderId);
}
