package com.fredy.pet_shop.repository;

import com.fredy.pet_shop.entity.Supervisor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupervisorRepository extends JpaRepository<Supervisor, String> {
    // Tidak perlu menambahkan custom query karena hanya mengikuti operasi CRUD dasar dari JpaRepository
}
