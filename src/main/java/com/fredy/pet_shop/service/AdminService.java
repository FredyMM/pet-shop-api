package com.fredy.pet_shop.service;

import com.fredy.pet_shop.entity.Admin;

import java.util.List;

public interface AdminService {

    List<Admin> getAll();
    Admin create(Admin admin);
    Admin getById(String adminId);
    Admin update(Admin admin);
    void deleteById(String adminId);
    Admin getByEmail(String email);
    List<Admin> searchAdmins(String keyword);
}
