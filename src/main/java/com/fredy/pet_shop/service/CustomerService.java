package com.fredy.pet_shop.service;

import com.fredy.pet_shop.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAll();
    Customer create(Customer customer);
    Customer getById(String customerId);
    Customer update(Customer customer);
    void deleteById(String customerId);
    Customer getByEmail(String email);
    List<Customer> searchCustomers(String keyword);
}
