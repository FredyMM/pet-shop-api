package com.fredy.pet_shop.service;

import com.fredy.pet_shop.entity.OrderDetail;

import java.util.List;

public interface OrderDetailService {

    List<OrderDetail> getAllOrderDetails();
    OrderDetail createOrderDetail(OrderDetail orderDetail);
    OrderDetail getOrderDetailById(String orderId);
    void deleteOrderDetail(String orderDetailId);
    List<OrderDetail> getOrderDetailsByOrderId(String orderId);
}
