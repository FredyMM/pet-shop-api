package com.fredy.pet_shop.service;

import com.fredy.pet_shop.entity.Order;
import com.fredy.pet_shop.entity.OrderDetail;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {

    List<Order> getAllOrders();
    Order createOrder(String customerId, List<OrderDetail> orderDetails);
    Order getOrderById(String orderId);
    List<Order> searchOrdersByDateRange(LocalDate startDate, LocalDate endDate);
}
