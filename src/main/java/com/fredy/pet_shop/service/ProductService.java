package com.fredy.pet_shop.service;

import com.fredy.pet_shop.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();
    Product createProduct(Product product);
    Product getProductById(String productId);
    Product updateProduct(Product product);
    void deleteProduct(String productId);
    List<Product> searchProducts(String keyword);
}
