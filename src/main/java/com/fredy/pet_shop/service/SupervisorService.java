package com.fredy.pet_shop.service;

import com.fredy.pet_shop.entity.Supervisor;

import java.util.List;

public interface SupervisorService {

    List<Supervisor> getAll();
    Supervisor create(Supervisor supervisor);
    Supervisor getById(String supervisorId);
    Supervisor update(Supervisor supervisor);
    void deleteById(String supervisorId);
}
