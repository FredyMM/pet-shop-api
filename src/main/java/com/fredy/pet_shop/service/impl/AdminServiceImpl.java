package com.fredy.pet_shop.service.impl;

import com.fredy.pet_shop.entity.Admin;
import com.fredy.pet_shop.repository.AdminRepository;
import com.fredy.pet_shop.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;

    @Override
    public List<Admin> getAll() {
        return adminRepository.findAll();
    }

    @Override
    public Admin create(Admin admin) {
        return adminRepository.save(admin);
    }

    @Override
    public Admin getById(String adminId) {
        return adminRepository.findById(adminId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Admin not found with ID: " + adminId));
    }

    @Override
    public Admin update(Admin admin) {
        if (!adminRepository.existsById(admin.getAdminId())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Admin not found with ID: " + admin.getAdminId());
        }

        return adminRepository.findById(admin.getAdminId())
                .map(existingAdmin -> {
                    existingAdmin.setName(admin.getName());
                    existingAdmin.setEmail(admin.getEmail());
                    return adminRepository.save(existingAdmin);
                })
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Admin not found with ID: " + admin.getAdminId()));
    }

    @Override
    public void deleteById(String adminId) {
        if (!adminRepository.existsById(adminId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Admin not found with ID: " + adminId);
        }
        adminRepository.deleteById(adminId);
    }

    @Override
    public Admin getByEmail(String email) {
        return adminRepository.findAdminByEmail(email)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Admin not found with email: " + email));
    }

    @Override
    public List<Admin> searchAdmins(String keyword) {
        List<Admin> admins = adminRepository.findAll();
        return admins.stream()
                .filter(admin -> admin.getName().toLowerCase().contains(keyword.toLowerCase()) || admin.getEmail().toLowerCase().contains(keyword.toLowerCase()))
                .collect(Collectors.toList());
    }
}
