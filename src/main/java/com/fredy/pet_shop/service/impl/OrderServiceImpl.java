package com.fredy.pet_shop.service.impl;

import com.fredy.pet_shop.entity.Customer;
import com.fredy.pet_shop.entity.Order;
import com.fredy.pet_shop.entity.OrderDetail;
import com.fredy.pet_shop.entity.Product;
import com.fredy.pet_shop.repository.CustomerRepository;
import com.fredy.pet_shop.repository.OrderDetailRepository;
import com.fredy.pet_shop.repository.OrderRepository;
import com.fredy.pet_shop.repository.ProductRepository;
import com.fredy.pet_shop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final OrderDetailRepository orderDetailRepository;

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order createOrder(String customerId, List<OrderDetail> orderDetails) {
        // Validate customer ID
        Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer not found with ID: " + customerId));

        // Calculate grand total and update product stock
        double grandTotal = 0.0;
        for (OrderDetail orderDetail : orderDetails) {
            Product product = productRepository.findById(orderDetail.getProduct().getProductId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found with ID: " + orderDetail.getProduct().getProductId()));

            if (product.getStock() < orderDetail.getQuantity()) {
                throw new RuntimeException("Insufficient stock for product: " + product.getName());
            }

            grandTotal += orderDetail.getSubtotal();
            product.setStock(product.getStock() - orderDetail.getQuantity());
            productRepository.save(product);
        }

        // Create New Order and set properties
        Order order = Order.builder()
                .customerId(customer.getCustomerId())
                .transactionDate(LocalDateTime.now())
                .grandTotal(grandTotal)
                .listOrderDetails(orderDetails)
                .build();

        // Save Order and OrderDetail
        order = orderRepository.saveAndFlush(order);
        for (OrderDetail orderDetail : orderDetails) {
            orderDetail.setOrder(order);
            orderDetailRepository.save(orderDetail);
        }

        return order;
    }

    @Override
    public Order getOrderById(String orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found with ID: " + orderId));
    }

    @Override
    public List<Order> searchOrdersByDateRange(LocalDate startDate, LocalDate endDate) {
        return orderRepository.searchOrdersByDateRange(startDate, endDate);
    }
}
