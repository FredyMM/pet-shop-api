package com.fredy.pet_shop.service.impl;

import com.fredy.pet_shop.entity.Supervisor;
import com.fredy.pet_shop.repository.SupervisorRepository;
import com.fredy.pet_shop.service.SupervisorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SupervisorServiceImpl implements SupervisorService {

    private final SupervisorRepository supervisorRepository;

    @Override
    public List<Supervisor> getAll() {
        return supervisorRepository.findAll();
    }

    @Override
    public Supervisor create(Supervisor supervisor) {
        return supervisorRepository.save(supervisor);
    }

    @Override
    public Supervisor getById(String supervisorId) {
        return supervisorRepository.findById(supervisorId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Supervisor not found with ID: " + supervisorId));
    }

    @Override
    public Supervisor update(Supervisor supervisor) {
        if (!supervisorRepository.existsById(supervisor.getSupervisorId())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Supervisor not found with ID: " + supervisor.getSupervisorId());
        }
        return supervisorRepository.save(supervisor);
    }

    @Override
    public void deleteById(String supervisorId) {
        if (!supervisorRepository.existsById(supervisorId)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Supervisor not found with ID: " + supervisorId);
        }
        supervisorRepository.deleteById(supervisorId);
    }
}
